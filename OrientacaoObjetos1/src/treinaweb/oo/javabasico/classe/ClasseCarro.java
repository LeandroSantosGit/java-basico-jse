package treinaweb.oo.javabasico.classe;

//criada apenas pra didatica

//exemplo de uma classe

public class ClasseCarro {
	
	//Atributos da classe
	public String cor = "";
	public String modelo = "";
	public String marca = "";
	public String motor = "";
	
	//Metodos da classe
	public void ligar() {
		System.out.println("Ligando Carro");
	}
	
	public void desligar() {
		System.out.println("Desligando Carro");
	}
	
	public void acelerar() {
		System.out.println("Acelerando Carro");
	}
	
	public void brecar() {
		System.out.println("Breacando Carro");
	}
	
	public void mudarMarcha() {
		System.out.println("Mudando Marchar do Carro");
	}

}
