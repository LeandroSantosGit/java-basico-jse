package treinaweb.oo.javabasico.app;

//criada apenas pra didatica

//instacia � uma copia da classe carro, com a instacia podemos criar varios objetos

//Objeto � uma instacia da ClasseCarro, apos criar o objeto ele sera armazenado na memoria

import treinaweb.oo.javabasico.classe.ClasseCarro;

public class Princiapal {

	public static void main(String[] args) {

		//criando o objeto
		ClasseCarro gol = new ClasseCarro(); //estamos instanciando a ClasseCarro e criando um objeto que sera gol
		ClasseCarro fusca = new ClasseCarro(); //estamos instanciando a ClasseCarro e criando um objeto que sera fusca
		ClasseCarro corsa = new ClasseCarro();
		
		//caracteristicas de carro
		gol.modelo = "Gol";
		gol.marca = "Volksvagem";
		gol.cor = "Preto";
		gol.motor = "1.0";
		
		fusca.modelo = "Fusca";
		fusca.marca = "Volksvagem";
		fusca.cor = "Branco";
		fusca.motor = "1.6";
		
		corsa.modelo = "Corsa";
		corsa.marca = "GM";
		corsa.cor = "Prata";
		corsa.motor = "1.4";
		
		
		System.out.println(gol.modelo + " " + gol.cor + " " + gol.motor);
		gol.ligar();
		gol.acelerar();
		gol.desligar();
		
		System.out.println("\n" + fusca.modelo + " " + fusca.cor + " " + fusca.motor);
		fusca.ligar();
		fusca.acelerar();
		fusca.brecar();
		fusca.desligar();
		
		System.out.println("\n" + corsa.modelo + " " + corsa.cor + " " + corsa.motor);
		corsa.ligar();
		corsa.acelerar();
		corsa.mudarMarcha();
		corsa.brecar();
		corsa.desligar();
		
		

	}

}
