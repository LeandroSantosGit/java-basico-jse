package treinaweb.javabasico.construtorclasse;

//criada apenas pra didatica

//Metodo construtor tem o mesmo nome da classe e nao tem tipo de retorno

public class ConstrutorClasse {
	
	public String estatus = "";
	
	public ConstrutorClasse() { //metodo construtor
		System.out.println("Construtor Chamado");
		estatus = "Iniciado";
	}
	
	public ConstrutorClasse(String estatus) {
		System.out.println("Chamado construtor com parametos");
		this.estatus = estatus;
	}

}
