package treinaweb.oo.javabasico.metodostatic;

//criada apenas pra didatica

//Metodo estatico pode ser chamado na classe sem a nesseciadade da criacao do objeto

public class Principal {

	public static void main(String[] args) {
		
		MetodoEstatico soma = new MetodoEstatico();
		
		int resultado = soma.ObterResultado(10, 25);
		
		System.out.println("Soma: " + resultado);
		
		resultado = MetodoEstatico.ObterResultado(25, 7); //chamado metodo estatico sem o objeto ser criado
		
		System.out.println("Resultado: " + resultado);
	}

}
