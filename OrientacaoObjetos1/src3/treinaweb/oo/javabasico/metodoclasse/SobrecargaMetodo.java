package treinaweb.oo.javabasico.metodoclasse;

//criada apenas pra didatica

//sobrecarga de metodos, sao metodos com nomes iguais mais com parametos diferente

public class SobrecargaMetodo {

	public static void main(String[] args) {
		
		//chamada do primeiro metodo1
		Imprimir();
		//chamada do segundo metodo2
		Imprimir("Claudia");
	}
	
	public static void Imprimir() { //metodo1
		System.out.println("Silvia");
	}
	
	public static void Imprimir(String nome) { //metodo2
		System.out.println(nome);
	}

}
