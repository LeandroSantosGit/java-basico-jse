package treinaweb.oo.javabasico.metodoclasse;

//criada apenas pra didatica, exemplo de metodo

//Metordo � procedimento ou um funca que representa o comprortamento da classe, podendo ou retorna um tipo de dado

import java.util.List;

public class Metodo {
	
	private int codigo;
	private String nome;
	private boolean ativo;
	
	public int getCodigo() {
		return codigo;
	}
	
	public void setCodigo(int codigo){
		this.codigo = codigo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public boolean isAtivo() {
		return ativo;
	}
	
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	public void alterarCliente() {
		//instrucoes para alterar cliente
	}
	
	public int obterNumeroClientes(){
	     int numeroClientes=0;
	     // Instru��es para obter o n�mero de clientes
	     return numeroClientes;
	}

	public int obterNumeroClientes(int situacao){
	    int numeroClientes=0;
	    //Instru��es para obter o n�mero de clientes
	    //em determinada situa��o:ativo e inativo
	    return numeroClientes;
	}

	public List<Metodo> listarClientes(){
		List<Metodo> lista = null;
		//Instru��es para obter a lista de clientes
		return lista;
	}

}
