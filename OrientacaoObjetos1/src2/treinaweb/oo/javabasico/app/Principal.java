package treinaweb.oo.javabasico.app;

//criada apenas pra didatica

import treinaweb.oo.javabasico.atributoestatico.ClasseEmpresa;

public class Principal {

	public static void main(String[] args) {

		//Primeiro objeto
		ClasseEmpresa autopeca = new ClasseEmpresa();
		
		autopeca.nome = "Rodrigo Auto Pecas";
		autopeca.ano = 2001;
		
		System.out.println(autopeca.nome);
		System.out.println(autopeca.ano);
		
		//Segundo objeto
		ClasseEmpresa oficina = new ClasseEmpresa();
		
		oficina.nome = "Oficina do Ze";
		//a variavel ano foi atribuida como estatica, entao a ano da sengun empresa sera o mesmo valor da empresa acima
		
		System.out.println(oficina.nome);
		System.out.println(oficina.ano);
		
		//Terceiro objeto
		ClasseEmpresa pinturaCar = new ClasseEmpresa();
		
		pinturaCar.nome = "Glass Tintas";
		pinturaCar.ano = 2010;
		
		System.out.println(pinturaCar.nome);;
		System.out.println(pinturaCar.ano);
	}

}
