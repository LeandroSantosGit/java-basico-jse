package treinaweb.principaisclasses.javabasico.classes;

//estudos das classes disponiveis no java

public class Classes {

	public static void main(String[] args) {
		
		//classe Integer, Integer classe empacotadora
		System.out.println("Classe Integer");
		System.out.println("Valor maximo para int: " + Integer.MAX_VALUE); //valor maximo de int
		System.out.println("Valor minimo para int: " + Integer.MIN_VALUE); //valor minimo de int
		int a = Integer.parseInt("1"); //converter uma string para um int
		System.out.println(Integer.toString(a)); //converte um int para string
		
		//classe Double
		System.out.println("\nClasse Double");
		System.out.println("Valor maximo para Doube: " + Double.MAX_VALUE); //valor maximo de double
		System.out.println("Valor minino para Double: " + Double.MIN_VALUE); //valor minimo de double
		double b = Double.parseDouble("2.5"); // converter uma string para double
		System.out.println(Double.toString(b)); // converter double para string
		
		//classe char
		System.out.println("\nClasse char");
		char c = '5';
		System.out.println(Character.isDigit(c)); // se o caracter for digito ira retornar verdadeiro
		System.out.println(Character.isLetter(c)); // se o caracter for uma letra ira retornar verdadeiro
		
	    //looper string 
		System.out.println("\nString");
		String nome = "LeandroSantos";
		System.out.println(nome.substring(4, 11)); //retornar apenas a posicao passada como parametro
		System.out.println(nome.toUpperCase()); // texto em caixa alta
		System.out.println(nome.toLowerCase()); // texto em caixa baixa
		
		

	}

}
