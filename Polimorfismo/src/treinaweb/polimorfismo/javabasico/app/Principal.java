package treinaweb.polimorfismo.javabasico.app;

import treinaweb.polimosfismo.javabasico.classes.Animal;
import treinaweb.polimosfismo.javabasico.classes.Cachorro;
import treinaweb.polimosfismo.javabasico.classes.Gato;

public class Principal {

	public static void main(String[] args) {
		
		Animal a;
		
		Gato g = new Gato();
		Cachorro c = new Cachorro();
		a = g;	
		a.som();
		a = c;
		a.som();
	}
}
