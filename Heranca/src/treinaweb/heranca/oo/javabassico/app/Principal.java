package treinaweb.heranca.oo.javabassico.app;

import treinaweb.heranca.oo.javabasico.classes.Animal;
import treinaweb.heranca.oo.javabasico.classes.Aves;
import treinaweb.heranca.oo.javabasico.classes.Mamifero;

public class Principal {

	public static void main(String[] args) {
		
		//criando os objetos e passando seus valores
		Mamifero cachorro = new Mamifero("Toto", 5, "Cachorro", 2);
		Aves arara = new Aves("Maroca", 2, "Arara", 6);
		Aves arara2 = new Aves("Maroca", 2, "Arara", 6);
		
		System.out.println(arara.equals(arara2));
		
		System.out.println(" ==================");
		arara.emitirBarrulho("hahahahah"); //esta chamndo o metodo modificado EmitirBarrulho da classe Aves
		cachorro.emitirBarrulho("hahahah"); //esta chamndo o metodo EmitirBarrulho da classe Aminal
		System.out.println(" ==================\n");
		
		
		try {
			
			System.out.println(cachorro.toString());
			cachorro.comer();
			cachorro.correr();
			System.out.println(cachorro.crescer());
			cachorro.dormir();
			
			System.out.println(arara.toString());
			arara.comer();
			arara.correr();
			arara.dormir();
			System.out.println(arara.crescer());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("\n\n-------------------------------");
		//System.out.println(arara.getClass()); //mostra o endereco da classe 
		//System.out.println(arara.toString()); //converte a representacao de objeto para formato de String, e mostra a posicao na memoria
		

	}

}
