package treinaweb.heranca.oo.javabasico.classes;

//public pode ser acessada por qualquer classe, inclusive por outros projetos
//private so pode ser acessado pela classe onde foi criado
//private so pode ser acessado pela classe que foi criado e as classes que herdam a classe de criacao


public abstract class Animal implements InterAnimal{
	
public static int QUANTIDADE_ANIMAIS = 0;
	
	//Atributos da classe
	private String nome = "";
	private int idade;
	private String especie = "";
	protected boolean estaVivo = true;
	
	
	
	public Animal(String nome, int idade, String especie) { //Metodo Construtor da classe
		this.nome = nome; //nome da classe igual a nome parametro
		this.idade = idade; //idade da classe igual a idade parametro
		this.especie = especie; //especie da classe igual a especia parametro
	} 

	//Metodos da classe
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) throws Exception {
		if(nome.equals("")) { //se nome for vazio tratar exececao
			throw new Exception("O nome nao pode estar vazio"); //nome nao pode ser vazio
		}
		
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getEspecie() {
		return especie;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}

	public boolean isEstaVivo() {
		return estaVivo;
	}

	public void setEstaVivo(boolean estaVivo) {
		this.estaVivo = estaVivo;
	}
	
	public String crescer() {
		return nome + " esta crescendo...";
	}
	
	public void dormir() {
		System.out.println(nome + " esta dormindo");
	}
	
	public void comer() {
		System.out.println(nome + " esta comendo");
	}
	 
	public final void emitirBarrulho() { //exemplo de sobrecarga de metodo, metodo final nao ser modificado em nenhuma classe
		System.out.println("Barruho do animal: nknllmlmlm");
	}
	
	public void emitirBarrulho(String barrulho) { //exemplo de sobrecarga de metodo
		System.out.println("Barrulho do animal: " + barrulho);
	}
	
	
	public static void verificarEstaVivo(Animal animal) {  //metodo estatico
		System.out.println(animal.isEstaVivo() ? animal.getNome() + " esta vivo!" : animal.getNome() + " morreu!");
	}                             //operador ternario, entao(?) se nao(:) 
	
	
	public abstract void correr();

}
