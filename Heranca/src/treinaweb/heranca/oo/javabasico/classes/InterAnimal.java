package treinaweb.heranca.oo.javabasico.classes;

//na interface serao definidos os metodos que serao implementados pelas classes derivadas
//toda interface ja � definada como publica por si so

public interface InterAnimal {
	
	String crescer();
	void dormir();
	void comer();
	void emitirBarrulho();
	void emitirBarrulho(String barulho);
	void correr();
	

}
