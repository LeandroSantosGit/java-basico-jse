package treinaweb.heranca.oo.javabasico.classes;

public final class Aves extends Animal {
	
	public Aves(String nome, int idade, String especie, int quantidadeOvos) {
		super(nome, idade, especie);
		this.quantidadeOvos = quantidadeOvos;
	}

	private int quantidadeOvos;

	public int getQuantidadeOvos() {
		return quantidadeOvos;
	}

	public void setQuantidadeOvos(int quantidadeOvos) {
		this.quantidadeOvos = quantidadeOvos;
	}

	@Override
	public String toString() { //modificar metodo toString da classe object
		
		return "Nome: " + this.getNome() + " Especie: " + this.getEspecie();
	}

	@Override
	public boolean equals(Object obj) { //metodo compara objetos e verificar se sao iguais

		Aves ave2 = (Aves)obj;  //criando um object para comparacao
		return this.getNome().equals(ave2.getNome()) &&            //comparar se nome, especie, idade, quantidadeOvos de Aves � 
				this.getEspecie().equals(ave2.getEspecie()) &&     // igual a ave2, se forem igual ira retornar verdadeiro.
				this.getIdade() == ave2.getIdade() &&
				this.getQuantidadeOvos() == ave2.getQuantidadeOvos();
	}

	@Override
	public int hashCode() { //criar um hash, um codigo unico para objeto
		
		return super.hashCode();
	}

	@Override
	public void emitirBarrulho(String barrulho) { //modificar o metodo EmitirBarrulho da classe Animal

		System.out.println(barrulho.toUpperCase());
	}

	@Override
	public void correr() {

		System.out.println("Ave esta correndo...");		
	}
	
	
	
	
	
	

}
