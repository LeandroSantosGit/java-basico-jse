package treinaweb.treads.javabasico.threads;

public class Tabuada implements Runnable{
	
	private int numero;
	
	public Tabuada(int numero) { //metodo construtor
		this.numero = numero;
	}
	
	@Override
	public void run() { //metodo de execucao de treads
		for(int i = 0; i <= 10; i++) { //
			System.out.printf("%d x %d = %d\n", numero, i, numero * 	i);
			try {
				Thread.sleep(1000); //alterar a execucao de cada thread para 1 segundo
			} catch (InterruptedException e) {//execao caso a thread possa ser interompida
				e.printStackTrace();
			}
		}
		
	}

}
