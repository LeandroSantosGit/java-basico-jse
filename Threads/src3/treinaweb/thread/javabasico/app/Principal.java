package treinaweb.thread.javabasico.app;

import treinaweb.thread.javabasico.classe.Tabuada;

public class Principal {

	public static void main(String[] args) {
		
		Tabuada tabuada5 = new Tabuada(5);
		Tabuada tabuada6 = new Tabuada(6);
		Tabuada tabuada7 = new Tabuada(7);
		
		//disparar treads
		Thread tr5 = new Thread(tabuada5); //criar thead para executar metodo runh
		Thread tr6 = new Thread(tabuada6);
		Thread tr7 = new Thread(tabuada7);
		
		//inicializacao da thread
		tr5.start();
		tr6.start();
		try {
			tr6.join(3000); //para travar a execucao da thread por determinado tempo
			tr6.interrupt(); //interroper a execucao da thread 
		} catch (InterruptedException e) { //tratar execao caso a thread possa ser interompida
			e.printStackTrace();
		}
		tr7.start();
		System.out.println("Threads Iniciadas");


	}


}
