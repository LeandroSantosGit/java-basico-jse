package treinaweb.thread.javabasico.classe;

public class Tabuada implements Runnable {

private int numero;
	
	public Tabuada(int numero) { //metodo construtor
		this.numero = numero;
	}
	
	@Override
	public void run() { //metodo de execucao de treads
		for(int i = 0; i <= 10; i++) { 
			if(Thread.interrupted()) { //se thread atual for interrompida sair da execucao
				return; //returnar apenas interrupcao
			}
			System.out.printf("%d x %d = %d\n", numero, i, numero * 	i);
			try {
				Thread.sleep(1000); //alterar a execucao de cada thread para 1 segundo
			} catch (InterruptedException e) {//execao caso a thread possa ser interompida
				return; //returnar apenas interrupcao
			}
		}
		
	}
}
