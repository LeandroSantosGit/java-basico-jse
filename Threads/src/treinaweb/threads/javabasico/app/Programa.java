package treinaweb.threads.javabasico.app;

import treinaweb.threads.javabasico.classe.TabuadaRunnable;

public class Programa {

	public static void main(String[] args) {
		
		//
		TabuadaRunnable tabuada5 = new TabuadaRunnable(5);
		TabuadaRunnable tabuada6 = new TabuadaRunnable(6);
		
		//disparar treads
		Thread tr5 = new Thread(tabuada5); //criar thead para executar metodo runh
		Thread tr6 = new Thread(tabuada6);
		
		//inicializacao da thread
		tr5.start();
		tr6.start();
		System.out.println("Threads Iniciadas");

	}

}
