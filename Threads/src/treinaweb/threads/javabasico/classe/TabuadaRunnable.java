package treinaweb.threads.javabasico.classe;

public class TabuadaRunnable implements Runnable {

	private int numero;
	
	public TabuadaRunnable(int numero) { //metodo construtor
		this.numero = numero;
	}
	
	@Override
	public void run() { //metodo de execucao de treads
		for(int i = 0; i <= 10; i++) { //
			System.out.printf("%d x %d = %d\n", numero, i, numero * 	i);
		}
		
	}

}
