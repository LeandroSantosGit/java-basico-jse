package treinaweb.collections.javabasico.hashset;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class Hashset {

	public static void main(String[] args) {

		Set<String> nomeAmigos = new HashSet<String>(); //criamos set com hashSet que trabalha com elementos unicos e nao permite duplicar
		String nome = "";
		
		Scanner leitor = new Scanner(System.in);
		
		while(!nome.equals("sair")) { //enquanto o nome nao for igual ao sair
			System.out.print("Digite o nome do amigo: ");
			nome = leitor.nextLine();
			
			if(!nome.equals("sair")) { //se nome nao for igual a sair
				nomeAmigos.add(nome); //adicionar elementos na lista
			}
		}
		
		System.out.println("\nLista de amigos");
		
		Iterator<String> it = nomeAmigos.iterator(); //iterator ira varer a lista 
		
		while(it.hasNext()) { //enquanto o iterator tiver elementos na lista, hasNext retorna verdadeiro se ainda estiver elementos  
			System.out.println(it.next()); //next ira retornar os elementos
		}
		

	}

}
