package treinaweb.collections.javabasico.hashmaps;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class HashMaps {

	public static void main(String[] args) {
		
		Map<String, Integer> notas = new HashMap<String, Integer>(); //mapa ultiliza chave(String) e valor(Integer), e nao tem elementos duplicados
		boolean continuar = true;

		
		
		Scanner leitor1 = new Scanner(System.in); //pegar oque o usuario digitar em string
		Scanner leitor2 = new Scanner(System.in); //pegar oque o usuario digitar em string
		
		while(continuar) { //enquanto continuar retornar elementos
			System.out.print("Digite nome do aluno: "); //usuario digitar o aluno
			String nomeAluno = leitor1.nextLine(); //passar o aluno para a variavel nomeAluno
			System.out.printf("Nota do aluno: ");
			int notaAluno = leitor2.nextInt();
			notas.put(nomeAluno, notaAluno);	
			System.out.printf("Deseja finalizar cadastro [S]im [N]ao: ");
			String terminarCadastro = leitor1.nextLine();
			
			if(terminarCadastro.equals("S")) { //se terminarcadastor for sim
				continuar = false; //terminar execucao
			}
		}
		
		System.out.println("\nAs notas da sala");
		
		for(String nomeAluno : notas.keySet()) { //forit - pegar notas com keySet que ira jogar os elementos para nomeAluno
			System.out.println(nomeAluno + " = " + notas.get(nomeAluno)); //imprimir ultilizando get para alunos com suas notas
		}
	}

}
