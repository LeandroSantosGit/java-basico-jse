package treinaweb.collections.javabasico.arraylist;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ArrayList2 {

	public static void main(String[] args) {
		
		List<String> nomeAmigos = new ArrayList<String>(); //ArrayList java generic espeficando que so pode receber string
		String nome = "sair";
		
		Scanner leitor = new Scanner(System.in);
		
		do {
			System.out.println("Digite nome do amigo: ");
			nome = leitor.nextLine();
			
			if(!nome.equals("sair")) {
				nomeAmigos.add(nome);
			}
		} while(!nome.equals("sair"));
		
		System.out.println("Lista de amigos");
		
		for(int i = 0; i < nomeAmigos.size(); i++) {
			System.out.println((String) nomeAmigos.get(i));
		}
		

	}

}
