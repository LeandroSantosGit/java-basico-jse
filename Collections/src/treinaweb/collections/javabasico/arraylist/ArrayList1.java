package treinaweb.collections.javabasico.arraylist;

import java.util.ArrayList; //importacao do ArrayList
import java.util.List;
import java.util.Scanner;

public class ArrayList1 {

	public static void main(String[] args) {
		
		                      //ArrayList armazenas os elementos na memoria de forma continua
		List nomesAmigos = new ArrayList(); //criamos o ArrayList, 
		String nome = "sair"; 
		
		Scanner leitor = new Scanner(System.in); //pegar oque o usuario digitar
		
		do {
			System.out.print("Digite o nome do amigo: "); 
			nome = leitor.nextLine(); //jogar na variavel nome oque o usuario digitar
			
			if(!nome.equals("sair")) { //se nao for diferente de sair
				nomesAmigos.add(nome); //adicionar os elementos na lista
			}
			
		} while(!nome.equals("sair")); //enquanto o nome nao for igual ao sair
		
		System.out.println("\nLista de amigos");
		
		for(int i = 0; i < nomesAmigos.size(); i++) { //varer a lista com metodo size pra retornar oque tem na lista
			System.out.println((String) nomesAmigos.get(i)); //imprimir a lista pegando os dados com get
		}
		
	}

}
