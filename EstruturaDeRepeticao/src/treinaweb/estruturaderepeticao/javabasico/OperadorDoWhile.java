package treinaweb.estruturaderepeticao.javabasico;

import java.util.Scanner;

public class OperadorDoWhile {

	public static void main(String[] args) {
		
		int contador = 0;
		String continua = "S";
		
		Scanner leitura = new Scanner(System.in);
		
		do { //ira executar a condicao a baixo pra depois o usuario dizer se continua ou nao
			System.out.printf("Valor do contador: %d\n", contador);
			System.out.print("Voce deseja continuar [S]im [N]ao: ");
			continua = leitura.nextLine();
			contador++;
		} while(continua.equals("S")); 
		
		System.out.println("Encerrado!"); 
	}

}
