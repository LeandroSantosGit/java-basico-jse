package treinaweb.estruturaderepeticao.javabasico;

import java.util.Scanner;

public class OperadorWhile {

	public static void main(String[] args) {
		
		int contador = 0;
		String continua = "S";
		
		Scanner leitura = new Scanner(System.in);
		
		while(continua.equals("S") && contador <= 4) { //se usuario digita S continuar a contagem E contagen vai ate 4
			System.out.printf("Valor do contador: %d\n", contador);
			System.out.print("Voce deseja continuar [S]sim [N]ao: ");
			continua = leitura.nextLine();
			contador++;
		}
		
		System.out.println("Encerrado!");

	}

}
