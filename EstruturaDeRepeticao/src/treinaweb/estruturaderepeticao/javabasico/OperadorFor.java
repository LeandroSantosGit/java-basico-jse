package treinaweb.estruturaderepeticao.javabasico;

import java.util.Scanner;

//tabuada de multiplicacao

public class OperadorFor {

	public static void main(String[] args) {
		
		int numero;
		int i = 0;
		
		Scanner leitura = new Scanner(System.in);
		
		System.out.print("Digite o numero para gerar Tabuada: ");
		numero = leitura.nextInt();
		
		for(i = 0; i <= 10; i++) {
			System.out.printf("%d X %d = %d\n", numero, i, numero * i);
		}
	}

}
