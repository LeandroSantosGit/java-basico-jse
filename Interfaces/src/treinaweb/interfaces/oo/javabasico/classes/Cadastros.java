package treinaweb.interfaces.oo.javabasico.classes;

//criada apenas pra didatica

//na interface serao definidos os metodos que serao implementados pelas classes derivadas  

public interface Cadastros {
	
	public void incluir();
	public void alterar();
	public void excluir(int codigo);

}
