package treinaweb.interfaces.oo.javabasico.classes;

//criada apenas pra didatica

//na interface serao definidos os metodos que serao implementados pelas classes derivadas  

public interface Enderecos {
	
	public String getEndereco();
	public void setEndereco(String endereco);
	public String getCep();
	public void setCep(String cep);
	public String getCidade();
	public void setCidade(String cidade);

}
