package treinaweb.vetores.javabasico;

public class ArrayCopy {

	public static void main(String[] args) {
		

		int num1[] = { 2, 4, 6, 8, 10 };
		int num2[] = new int[5];

		num2[0] = 1;
		num2[1] = 3;
		num2[2] = 5;
		num2[3] = 7;
		num2[4] = 9;

		int num[] = new int[10];

		System.arraycopy(num1, 0, num, 0, 5); // copiando os elemento do arry num1 para num

		System.arraycopy(num2, 0, num, 4, 5); // copiando os elemento do arry num2 para num

		for (int i = 0; i < num.length; i++) {
			System.out.printf("num[%d] = %d\n", i, num[i]); // imprimir a lista de elementos
		}

	}

}
