package treinaweb.vetores.javabasico;

//lista de compras

import java.util.Scanner;

public class Array {

	public static void main(String[] args) {
		
		String listaCompras[] = new String[5];
		int i = 0;
		
		Scanner leitor = new Scanner(System.in);
		
		for(i = 0; i <= 4; i++) {
			System.out.printf("Digite oque vai comprar: ");
			listaCompras[i] = leitor.nextLine();
		}
		
		System.out.println("\nSua lista de compras");
		
		for(i = 0; i <= 4; i++) {
			System.out.println("Mercadoria: " + listaCompras[i]);
		}

	}

}
