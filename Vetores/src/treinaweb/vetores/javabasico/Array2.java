package treinaweb.vetores.javabasico;

import java.util.Arrays;
import java.util.Scanner;

public class Array2 {

	public static void main(String[] args) {

		String listaAmigos1[] = new String[5];
		String listaAmigos2[] = new String[5];
		int i = 0;
		
		Scanner leitor = new Scanner(System.in);
		
		for(i = 0; i <= 4; i++) {
			System.out.print("Digite o nome de seus amigo: ");
			listaAmigos1[i] = leitor.nextLine();
			listaAmigos2[i] = listaAmigos1[i]; //preencher lista2 com os mesmo elementos da lista1
		}
		
		System.out.println("\nLista de seus amigos");
		
		Arrays.sort(listaAmigos1); //colocar a lista em ordem ordenada
		Arrays.sort(listaAmigos2);
		
		for(i = 0; i <= 4; i++) {
			System.out.println("Amigo: " + listaAmigos1[i]);
		}
		
		System.out.println("\n--------------------------\n");
		System.out.printf("Tamanho do vetor lista1: " + listaAmigos1.length); //mostra o tamanho que foi declarado para o vetor 
		System.out.printf("\nTamanho do vetor lista2: " + listaAmigos2.length);
		
		//comparar as posicoes dos elementos no vetor
		System.out.println("\nOs vetores sao iguais: " + Arrays.equals(listaAmigos1, listaAmigos2)); 
		
		//mostrar qual a posicao do vetor um elemento esta cadastrado
		System.out.println("A posicao do joao na lista �: " + Arrays.binarySearch(listaAmigos1, "Joao"));
	}

}
