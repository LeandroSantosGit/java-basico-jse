package treinaweb.classesgenericas.oo.javabasico.generic;

import java.util.ArrayList;
import java.util.List;

import treinaweb.classesgenericas.oo.javabasico.classes.Animal;

public class ManipuladorAnimais<T extends Animal> { //parametro para manipular animais
	
	private List<T> animais = new ArrayList<T>(); //criando lista
	
	public List<T> getAnimais(){ //metodo para retornar animais
		return animais;
	}
	
	public T getProPocisao(int indice) { //metodo ira retorar pocisao de animais
		return animais.get(indice);
	}
	
	public void inserirAnimal(T animal) { //metodo para inserir animais 
		animais.add(animal);
	}
	

}
