package treinaweb.classesgenericas.oo.javabasico.app;

import java.util.List;

import treinaweb.classesgenericas.oo.javabasico.classes.Aves;
import treinaweb.classesgenericas.oo.javabasico.classes.Mamifero;
import treinaweb.classesgenericas.oo.javabasico.generic.ManipuladorAnimais;
import treinaweb.classesgenericas.oo.javabasico.generic.ManipuladorAves;

public class Principal {

	public static void main(String[] args) {

		ManipuladorAves manipuladorAves = new ManipuladorAves();//criar objeto manipulador, estamos ManipuladorAves pk ele extends o ManipuladorAnimais 
		ManipuladorAnimais<Mamifero> manipuladorMamifero = new ManipuladorAnimais<Mamifero>();
		
		Aves ave = new Aves("Maroca", 5, "Arara", 6); //passar os elementos 
		System.out.println("Inserir ave");
		manipuladorAves.inserirAnimal(ave); //inserir elemento ultilizando o metodo inserir
		
		Aves aveRecuperar = manipuladorAves.getProPocisao(0); //recuperar a posicao 
		System.out.println("Ave recuperada: " + aveRecuperar.getNome());
		List<Aves> aves = manipuladorAves.getAnimais(); //lista de ave
		
		for(Aves a : aves) {
			System.out.println("Ave recuperada: " + a.getNome());
		}
		
		Mamifero mamifero = new Mamifero("Toto", 5, "Cachorro", 6); //passar os elementos 
		System.out.println("Inserir mamifero");
		manipuladorMamifero.inserirAnimal(mamifero); 
		
		Mamifero mamiferoRecuperar = manipuladorMamifero.getProPocisao(0); //recuperar a posicao 
		System.out.println("Mamifero recuperada: " + mamiferoRecuperar.getNome());
		List<Mamifero> mamiferos = manipuladorMamifero.getAnimais(); //lista de mamifero
		
		
		for(Mamifero m : mamiferos) {
			System.out.println("Mamifero recuperada: " + m.getNome());
		}
	}

}
