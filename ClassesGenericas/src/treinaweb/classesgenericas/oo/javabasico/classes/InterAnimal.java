package treinaweb.classesgenericas.oo.javabasico.classes;

public interface InterAnimal {
	
	String crescer();
	void dormir();
	void comer();
	void emitirBarrulho();
	void emitirBarrulho(String barulho);
	void correr();

}
