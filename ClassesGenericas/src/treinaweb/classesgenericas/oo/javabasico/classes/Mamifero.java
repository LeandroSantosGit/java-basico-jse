package treinaweb.classesgenericas.oo.javabasico.classes;

public final class Mamifero extends Animal{
	
	public Mamifero(String nome, int idade, String especie, double tamanhoCauda) {
		super(nome, idade, especie);
		this.tamanhoCauda = tamanhoCauda;
	}

	private double tamanhoCauda;

	public double getTamanhoCauda() {
		return tamanhoCauda;
	}

	public void setTamanhoCauda(double tamanhoCauda) {
		this.tamanhoCauda = tamanhoCauda;
	}

	@Override
	public String toString() {
		
		return "Nome: " + this.getNome() + " Especie: " + this.getEspecie();
	}

	@Override
	public void correr() {
		
		System.out.println("Mamifero esta correndo...");		
	}
	

}
