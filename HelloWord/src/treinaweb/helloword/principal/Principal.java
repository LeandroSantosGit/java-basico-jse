package treinaweb.helloword.principal;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) { //metodo principal
		
		System.out.println("Hello Word!\n");  //imprimir
		
		System.out.println("----------------------------------------\n");
		
		String nomePessoa = "Leandro Santos";
		int idadePessoa = 26;
		double pesoPessoa = 81.5;
		
		
		System.out.println("Primeiro Programa em java!\n");
		System.out.println("Nome: " + nomePessoa);
		System.out.println("Idade: " + idadePessoa + " anos.");
		System.out.println("Peso: " + pesoPessoa + " kilos.");
		
		System.out.println("-----------------------------------------\n");
		
		Scanner leitor = new Scanner(System.in); //coloar para o usuario digitar
		System.out.println("Digite sua idade: ");
		
		String idade = leitor.nextLine(); //colocar oque o usuario digitou e colocar na variavel idade
		int idadeConvertida = Integer.parseInt(idade); //converter string idade para int
	//	int idade = leitor.nextInt(); // forma mais facil de converter string idade para int neste caso
		
		if(idadeConvertida >= 18) { //comparar se usuario e maior de idade
			System.out.println("Usuario maior de idade.");
		} else {
			System.out.println("Usuario nao � maior de idade.");
		}
		
		System.out.println("------------------------------------------\n");
		
		System.out.println("Converter dados\n");
		
		double numero1 = 1.5;
		int numero2 = (int) numero1; //converter double para int 
		
		System.out.println("Valor de numero1: " + numero1);
		System.out.println("Valor de numero1 convertido: " + numero2);
		
		int a = 2;
		double b = a; //foi convertido int para double
		
		System.out.println("Valor de a: " + a);
		System.out.println("Valor de b convertido: " + b);

	}

}
