package treinaweb.operadores.javabasico;

public class IncrementoDecremento {

	public static void main(String[] args) {
		
		int i = 0;
		int y = 0;
		
		System.out.println("---Incremento---");
		System.out.println("Valor de i = " + i);
		System.out.println("Valor de i++ = " +  i++); //incrementando depois da chamada da variavel
		System.out.println("Valor de i = " + i);
		System.out.println("Valor de ++i = " + ++i);  //incrementando antes da chamada da variavel
		System.out.println("Valor de i = " + i);
		
		System.out.println("\n---Incremento---");
		System.out.println("Valor de y = " + y);
		System.out.println("Valor de y++ = " + y--); //decrementando depois da chamada da variavel
		System.out.println("Valor de y = " + y);
		System.out.println("Valor de ++y = " + --y); //decrementando antes da chamada da variavel
		System.out.println("Valor de y = " + y);

	}

}
