package treinaweb.operadores.javabasico;

public class OperadoresAritmetico {

	public static void main(String[] args) {
		
		int numero1 = 5;
		int numero2 = 4;
		
		System.out.printf("Soma: %d + %d = %d\n", numero1, numero2, numero1 + numero2);
		System.out.printf("Subtracao: %d - %d = %d\n", numero1, numero2, numero1 - numero2);
		System.out.printf("Multiplicacao: %d * %d = %d\n", numero1, numero2, numero1 * numero2);
		System.out.printf("Divisao: %d / %d = %d\n", numero1, numero2, numero1 / numero2);
		System.out.printf("Resto divisao: %d e %d = %d\n", numero1, numero2, numero1 % numero2);
		
	}

}
