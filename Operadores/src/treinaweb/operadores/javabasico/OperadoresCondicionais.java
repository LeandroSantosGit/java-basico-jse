package treinaweb.operadores.javabasico;

public class OperadoresCondicionais {

	public static void main(String[] args) {
		
		int a = 3;
		int b = 5;
		
		System.out.println("---Operador E(&&)---"); //esse operador so sera verdadeiro se as duas condicoes forem verdadeiras
		System.out.printf("%d = 3 e %d = 5 -> %b\n", a, b, a == 3 && b == 5); //se a � igual a 3 E se b for igual a 5
		System.out.printf("%d = 4 e %d = 5 -> %b\n", a, b, a == 4 && b == 5); //se a � igual a 4 E se b for igual a 5
		System.out.printf("%d = 3 e %d = 2 -> %b\n", a, b, a == 3 && b == 2); //se a � igual a 3 E se b for igual a 2
		System.out.printf("%d = 8 e %d = 6 -> %b \n\n", a, b, a == 8 && b == 6); //se a � igual a 8 E se b for igual a 6
		
		System.out.println("---Operador OU(||)---"); //esse operador so sera falso se as duas condicoes forem falsas
		System.out.printf("%d = 3 ou %d = 5 -> %b\n", a, b, a == 3 || b == 5);
		System.out.printf("%d = 4 ou %d = 5 -> %b\n", a, b, a == 4 || b == 5);
		System.out.printf("%d = 3 ou %d = 2 -> %b\n", a, b, a == 3 || b == 2);
		System.out.printf("%d = 8 ou %d = 6 -> %b\n\n", a, b, a == 8 || b == 6);
		
		System.out.println("---Operador NEGACAO(!)---"); //esse operador retorna  inverso negando a condicao real
		System.out.printf("%d == 3\n", a, !(a ==3)); //exepressao verdadeira mais vai retorna falso 
		System.out.printf("%d == 5\n", b, !(b == 5)); //exepressao verdadeira mais vai retorna falso

		
	}

}
