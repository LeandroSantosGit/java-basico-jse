package treinaweb.operadores.javabasico;

public class OperadoresLogicos {
	
	
	static int LEITURA = 1;
	static int ALTERACAO = 2;
	
	public static void main(String[] args) {
		
		int permissoes = 0; // 00 
		
		System.out.println("Permissao Inicial: " + Integer.toBinaryString(permissoes)); // 00
		permissoes = permissoes | LEITURA; // |01
		System.out.println("Permissao apos Leitura: " + Integer.toBinaryString(permissoes)); // 01
		permissoes = permissoes | ALTERACAO; // |10
		System.out.println("Permissao apos Alteracao: " + Integer.toBinaryString(permissoes)); // 11
		
		if((permissoes & LEITURA) == LEITURA) { //verificar se existe a permisao de leitura
			System.out.println("Tenho permissao de alteracao");
		} else {
			System.out.println("Nao tenho permissao de leitura");
		}
		
		if((permissoes & ALTERACAO) == ALTERACAO) { //verificar se existe a permisao de alteracao
			System.out.println("Tenho permissao de alteracao");
		} else {
			System.out.println("Nao tenho permissao de alteracao");
		}
		

	}

}
