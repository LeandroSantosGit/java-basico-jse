package treinaweb.operadores.javabasico;

public class OperadoresRelacionais {

	public static void main(String[] args) {
		
		int a = 8;
		int b = 5;
		
		System.out.printf("%d > %d = %b\n", a, b, a > b); //a maior que b
		System.out.printf("%d < %d = %b\n", a, b, a < b); //a menor que b
		System.out.printf("%d >= %d = %b\n", a, b, a >= b); //a maior ou igual b
		System.out.printf("%d <= %d = %b\n", a, b, a <= b); //a menor ou igual a b
		System.out.printf("%d = %d = %b\n", a, b, a == b); //a e igual b
		System.out.printf("%d <> %d = %b\n", a, b, a != b); //a � diferente de b
		
		
	}

}
