package treinaweb.operadores.javabasico;

public class OperacaoBit {

	public static void main(String[] args) {
		
		int a = 2;
		
		System.out.println(Integer.toBinaryString(a)); //base binaria da variavel a
		
		int b = a >> 1; //rotasionar para a direita a base binaria
		
		System.out.println(Integer.toBinaryString(b));
		
		int c = a << 1; //rotacionar para a esquerda a base binaria
		
		System.out.println(Integer.toBinaryString(c));
		
		System.out.println("a = " +a);
		System.out.println("b = " +b);
		System.out.println("c = " +c);

	}

}
