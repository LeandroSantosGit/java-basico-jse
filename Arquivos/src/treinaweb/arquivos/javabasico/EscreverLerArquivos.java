package treinaweb.arquivos.javabasico;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

public class EscreverLerArquivos {

	public static void main(String[] args) {
		
		//texto para ser escrito nos arquivos
		String texto = "Conteudo escrito nos arquivos";
		
		//executar metodos para escrever os arquivos 
		escreverArquivoOutputStream(texto);
		escreverArquivoFiles(texto);
		escreverArquivoBufferedWriter(texto);
		escreverArquivoFileWriter(texto);
		System.out.println("Terminou de escrever os arquivos.\n");
		
		//executar metodos para ler arquivos
		lerArquivoOutputStream();
		lerArquivoFiles();
		lerArquivoBufferedReader();
		lerArquivoFileReader();
		System.out.println("Terminou de ler arquivos.");

	}
	
	//escrever arquivo OutputStream
	private static void escreverArquivoOutputStream(String texto) { //OutputStream metodo para criar arquivo e escrever
		File arquivo = new File("Arquivo_output.txt"); //criar arquivo para referencia escrita
		
		if(!arquivo.exists()) { //verificar se arquivo ja existe, se nao existir sera criado
			try {
				arquivo.createNewFile(); //criar arquivo
			} catch (IOException e) { //tratar execao caso nao tenha permissao para criar arquivo
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		OutputStream stream = null; 
		try {
			stream = new FileOutputStream(arquivo); //criar objeto para escrever
			stream.write(texto.getBytes(), 0, texto.length()); //escrever no arquivo desde o primeiro byte ate o total do texto
		} catch (FileNotFoundException e) { //tratar execao caso o arquivo nao exista
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) { //tratar execao caso nao tenha permissao para escrever no arquivo
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally { //finalizar stream
			if(stream != null) { //se for diferente de nullo fechar
				try {
					stream.close(); //fechar stream
				} catch (IOException e) { //tratar caso nao tenha permissao para fechar
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	
	//ler aquivo com OutputStream
	private static void lerArquivoOutputStream() {
		File arquivo = new File("Arquivo_output.txt");	//arquivo para leitura
		
		InputStream is = null;
		try {
			is = new FileInputStream(arquivo); //buscar o conteudo
			byte[] conteudo = new byte[100]; //ler o arquivo em byte
			is.read(conteudo); //adicionar a leitura em conteudo
			String conteudoArquivo = new String(conteudo); //converter de bayte para string para imprimir 
			System.out.println(" ---- Lendo o conteudo InputStream ----");
			System.out.println(conteudoArquivo); //imprimir o conteudo do arquivo
			System.out.println("===================");
		} catch (FileNotFoundException e) { //tratar caso o arquivo nao exista
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) { //tratar caso nao tenha permissao para ler arquivo
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally { //finalizar InputStream
			if(is != null) { //se InputStream for diferente de nulo
				try {
					is.close(); //fechar InputStream
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	
	//escrever arquivo com Files
	private static void escreverArquivoFiles(String texto) { //Files metodo para criar arquivo e escrever
		File arquivo = new File("arquivo_files.txt"); //criar arquivo para referencia escrita
		
		if(!arquivo.exists()) { //verificar se arquivo ja existe, se nao existir sera criado
			try {
				arquivo.createNewFile(); //criar arquivo
			} catch (IOException e) { //tratar execao caso nao tenha permissao para criar arquivo
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			Files.write(Paths.get(arquivo.getAbsolutePath()), texto.getBytes()); //criar objeto para escrever, passando o caminho do arquivo e o texto a ser escrito
		} catch (IOException e) { //tratar execao caso nao tenha permissao para escrever no arquivo
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//ler arquivo com File
	private static void lerArquivoFiles() {
		File arquivo = new File("arquivo_files.txt"); //arquivo para leitura
		
		try {
			String conteudoArquivo = new String(Files.readAllBytes(Paths.get(arquivo.getAbsolutePath()))); //buscar o conteudo indo no caminho onde esta o arquivo
			System.out.println("---- Lendo o arquivo com Flies ----");
			System.out.println(conteudoArquivo);
			System.out.println("========================");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//escrever arquivo com BufferedWriter
	private static void escreverArquivoBufferedWriter(String texto) { //BufferedWriter metodo para criar arquivo e escrever
		File arquivo = new File("arquivo_bufferedwriter.txt"); //criar arquivo para referencia escrita
		
		if(!arquivo.exists()) { //verificar se arquivo ja existe, se nao existir sera criado
			try {
				arquivo.createNewFile(); //criar arquivo
			} catch (IOException e) { //tratar execao caso nao tenha permissao para criar arquivo
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		Writer fw = null; 
		BufferedWriter bw = null;
		try {
			 fw = new FileWriter(arquivo); //escrever no arquivo
			 bw = new BufferedWriter(fw); //atraves do BufferedWriter que ira ser feita a escrita do arquivo que foi passado(fw)
			 bw.write(texto); //escrever o texto passsado
		} catch (IOException e) { //tratar execao caso nao tenha permissao para escrever no arquivo
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally { //finaluzar BufferedWriter
			if(bw != null) { //se BufferedWriter for diferente de null
				try {
					bw.close(); //fechar BufferedWriter
				} catch (IOException e) { //tratar caso nao tenha permissao para fechar	
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(fw != null) { //finaluzar FileWriter
				try {
					fw.close(); //fechar FileWriter
				} catch (IOException e) { //tratar caso nao tenha permissao para fechar	
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	
	//ler arquivo BufferedReader
	private static void lerArquivoBufferedReader() {
		File arquivo = new File("arquivo_bufferedwriter.txt"); //arquivo para leitura
		
		BufferedReader bf = null;
		try {
			bf = new BufferedReader(new FileReader(arquivo)); // buscar arquivo para leitura
			String linha = ""; //linha padrao que comecara em branco
			System.out.println("---- Lendo com BufferedReader ----");
			while((linha = bf.readLine()) != null) {  // varrer o arquivo e pegar linha a linha o conteudo enquanto for diferente de nulo
				System.out.println(linha); //a cada inha imprimir
			}
			System.out.println("=====================");
		} catch (FileNotFoundException e) { //tratar caso o arquivo nao exista
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) { //tratar caso nao tenha conteudo
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally { //fechar BufferedReader
			if(bf != null) {
				try {
					bf.close(); //fechar BufferedReader
				} catch (IOException e) { //tratar caso seja permitido fechar
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			
		}
	}
	
	//escrever arquivo com FileWriter
	private static void escreverArquivoFileWriter(String texto) { //FileWriter metodo para criar arquivo e escrever
		File arquivo = new File("arquivo_filewriter.txt"); //criar arquivo para referencia escrita
		
		if(!arquivo.exists()) { //verificar se arquivo ja existe, se nao existir sera criado
			try {
				arquivo.createNewFile(); //criar arquivo
			} catch (IOException e) { //tratar execao caso nao tenha permissao para criar arquivo
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		Writer fw = null;
		try {
			fw = new FileWriter(arquivo); //escrever no arquivo
			fw.write(texto); //escrever o texto passsado
		} catch (IOException e) { //tratar caso nao tenha permissao para escrever
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally { //finalizar Writer
			try {
				fw.close(); //fechar Writer
			} catch (IOException e) { //tratar caso nao tenha permissao para fechar	
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	//ler arquivo FileReader
	private static void lerArquivoFileReader() {
		File arquivo = new File("arquivo_filewriter.txt"); //arquivo para leitura
		
		char[] conteudo = new char[100];
		Reader reader = null;
		try {
			reader = new FileReader(arquivo); 
			System.out.println("---- Lendo com FileReader----");
			while(reader.read(conteudo) != -1) { //jogar conteudo para arry, enquanto conteudo for diferente de -1 caracter
				System.out.println(String.copyValueOf(conteudo)); //imprimir conteudo
			}
			System.out.println("============================");
		} catch (FileNotFoundException e) { //tratar caso o arquivo nao exista
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) { 
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
