package treinaweb.interpolacao.principal;

public class Principal {

	public static void main(String[] args) {
		
		int a = 3;
		int b = 5;
		int c = 10;
				
		int resultado = (a * b) + c;
		
		System.out.println("O resultado de " + a + " x " + b + " + " + c + " e " + resultado);
		
		//vamos ultilizar printf para exibir um texto formatado passa typechar que seram substituidos pelos argumentos 
		System.out.printf("O resultado de %d x %d + %d e %d", a, b, c, resultado);
		
		
	}

}
