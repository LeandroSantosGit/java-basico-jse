package treinaweb.classesabstratas.oo.javabasico.app;

import treinaweb.classesabstratas.oo.javabasico.classes.Jogo;

public class Principal {

	public static void main(String[] args) {

		Jogo j = new Jogo();
		
		j.carregarDados();
		j.processarDados();
		j.removerDados();
	}

}
