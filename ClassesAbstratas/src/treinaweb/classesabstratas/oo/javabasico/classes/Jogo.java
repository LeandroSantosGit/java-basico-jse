package treinaweb.classesabstratas.oo.javabasico.classes;

//essa classe e filha da classe Game, aqui sera sobreescrito os metodos definidos na classe pai

public class Jogo extends Game{

	//sobreecrevendo metodos
	@Override
	public void carregarDados() {
		
		System.out.println("Carregar Jogo...");
	}

	@Override
	public void processarDados() {

		System.out.println("Processando Dados do Jogo...");
	}

	@Override
	public void removerDados() {
		
		System.out.println("Removendo dados do Jogo...");
	}

}
