package treinaweb.classesabstratas.oo.javabasico.classes;

//classe abstrata que nao pode ser instaciada, ela define metodos que serao declarados nas classes filhas

public abstract class Game {
	
	private boolean ativo = true;
	public abstract void carregarDados(); //metodos abstratos serao sobreescrito nas classes filhas
	public abstract void processarDados();
	public abstract void removerDados();
	
	public void run() {
		carregarDados();
		
		while(ativo) {
			processarDados();
		}
		
		removerDados();
	}
	
	public void parar() {
		this.ativo = false;
	}
	

}
