package treinaweb.jdbc.javabasico.app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import treinaweb.jdbc.javabasico.mysql.ConexaoMysql;

public class Principal {

	public static void main(String[] args) {
		
		try {
			//executar conexao
			Connection connection = ConexaoMysql.criarConexao(); 
			System.out.println("Conexao com sucesso");
			
			//inserir no bando de dados
			PreparedStatement statement = ConexaoMysql.criarPreparedStatement(connection, "INSERT INTO pessoas (nome, idade) VALUES(?, ?)");
			statement.setString(1, "Leandro"); //substituir as interrogacoes pelo valores definido 
			statement.setInt(2, 26);
			statement.execute(); //executar a insercao
			
			//consultar bando de dados 
			ResultSet rs = ConexaoMysql.executarSelect(connection, "SELECT * FROM pessoas"); //efeturar consulta no banco
			System.out.println("Pessoas no banco de dasdos");
			while(rs.next()) { //enquanto tiver linha continue retornando pra serem lidas
				System.out.println(rs.getInt("id")); //varrer do banco para imprimir
				System.out.println(rs.getString("nome"));
				System.out.println(rs.getInt("idade"));
				System.out.println("---------------------");
			}
			System.out.println("Fim da leitura do banco de dados.");
			
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}

}
