package treinaweb.jdbc.javabasico.mysql;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;

public class ConexaoMysql {
	 //tratar com throws  caso nao criar class, caso nao tenha permissao de acessar blibiotecas, caso a classe nao exista, caso ocorra erro ao acessar banco de dados
	public static Connection criarConexao() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException { //iniciar conexao
		
		Class.forName("com.mysql.jdbc.Driver").newInstance(); //chamar drive do mysql	
		String url = "jdbc:mysql://127.0.0.1:3307/treinaweb_jsebasico?user=root&password=1234"; //string de conexao para criar conexao
		Connection connection = DriverManager.getConnection(url); //criar conexao
		return connection; //returnar a conexao para acesso
	}
	
	public static ResultSet executarSelect(Connection conn, String sql) throws SQLException {
		Statement query = conn.createStatement(); //objeto para executar comandos sql
		
		return query.executeQuery(sql); //retornar a consulta do sql
		
	}
	
	//metodo para inserir ultilizando parametro PreparedStatement
	public static PreparedStatement criarPreparedStatement(Connection conn, String sql) throws SQLException {
		return conn.prepareStatement(sql);
	}

}
