package treinaweb.estruturadedecisao.javabasico;

//controle de fluxo � controlar oque sera executado pelo programa

public class ControleFluxo {

	public static void main(String[] args) {
		
		int ano = 0;
		
		ano = 2012;
		
		if(ano == 2012) {
			//somente sera executado se o ano for 2012
			System.out.println("O ano � 2012");
		} else {
			//sera executado se o ano nao for 2012
			System.out.println("O ano nao � 2012");
		}

	}

}
