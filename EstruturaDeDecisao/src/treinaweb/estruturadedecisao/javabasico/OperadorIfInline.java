package treinaweb.estruturadedecisao.javabasico;

//If Inline

public class OperadorIfInline {

	public static void main(String[] args) {
		
		int nota = 8;
		
		System.out.println(nota > 7 ? "Aprovado" : "Reprovado");

	}

}

// ? se
// : se nao
