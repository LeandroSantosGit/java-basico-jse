package treinaweb.estruturadedecisao.javabasico;

import java.util.Scanner;

public class OperadorIfElseIfelse {

	public static void main(String[] args) {
		
		int idade;
		String sexo;
		
		Scanner leitor1 = new Scanner(System.in);
		Scanner leitor2 = new Scanner(System.in);
		
		System.out.println("Digite idade: ");
		idade = leitor1.nextInt();
		System.out.println("Digite [M] Masculino [F] Feminino");
		sexo = leitor2.nextLine();
		
		if(sexo.equals("M")) {
			if(idade >= 21) {
				System.out.println("Voce nao ira crescer mais.");
			} else {
				System.out.println("Voce ira crescer mais.");
			}
		} else if(sexo.equals("F")){
			if(idade >= 18) {
				System.out.println("Voce nao ira crescer mais.");
			}else {
				System.out.println("Voce ira crescer mais.");
			}
		} else {
			System.out.println("Opcao Invalida!");
		}
	}

}
