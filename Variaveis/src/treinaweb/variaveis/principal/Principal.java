package treinaweb.variaveis.principal;

public class Principal {
	//variavel da classe
	static String exemplo = "Variavel da classe";

	public static void main(String[] args) {
		
		int i = 20; //variavel do metodo
		 
		if(i > 10) { 
			int x = 10; //variavel local
			
			System.out.println(exemplo);
			System.out.println("Variavel do metodo: " + i);
			System.out.println("Variavel local: " + x);
		}
		
		Calculadora calc = new Calculadora(); //chamar a classe Calculadora dentro da calsse principal(main)
		System.out.println("\nValor da soma: " + calc.soma() + "\n");
		
		VariavelFinal variaFim = new VariavelFinal(); //chamar a classe VariavelFinal dentro da calsse principal(main)
		System.out.println("O valor da variavel final: " + variaFim.PI);
		
	}

}
