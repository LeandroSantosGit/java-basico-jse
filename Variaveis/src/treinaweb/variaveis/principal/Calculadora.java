package treinaweb.variaveis.principal;

public class Calculadora {
	
	public int soma() {
		
		int numero1 = 2; //
		int numero2 = 2;
		
		if(verificar(numero1, numero2)) { //passar a e b para numero1 e numero2 e chamar a funcao verificar
			return numero1 + numero2; //se maior que zero efetuar a soma
		} else {
			return 0;
		}
	}
	
	public boolean verificar(int a, int b) { //variavel como parametros
		return a > 0 && b > 0;  //a maior que zero e b maior que zero
	}

}
