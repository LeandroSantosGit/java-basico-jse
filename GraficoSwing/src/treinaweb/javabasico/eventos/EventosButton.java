package treinaweb.javabasico.eventos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class EventosButton implements ActionListener{
	
	private JTextField textField;
	
	public EventosButton(JTextField textField) { //alterando metodo construtor da classe
		this.textField = textField;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) { //vento que ira ocorrer quando usuario digitar um texto
		
		JOptionPane.showMessageDialog(null, "Voce digitou: " + textField.getText()); //messagem e o texto que o usuario digitou
	}

}
