package treinaweb.javabasico.eventos;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class EventoTextField implements KeyListener{ //capturar caracteres digitados no campo de texto

	@Override
	public void keyPressed(KeyEvent arg0) { //metodo ocorre quando determinada tecla e apertada
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) { //ocorre quando soltamos determinada tecla
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) { //ocorre quando apermos tecla alfanumerico 

		char tecla = e.getKeyChar(); //pegar a tecla presionada
		
		if(tecla >= '0' && tecla <= '9') { //se tecla estiver entre 0 e 9
			e.consume(); //nao permitir numeros serem digitados
		}
		
	}

}
