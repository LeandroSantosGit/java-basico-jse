package treinaweb.javabasico.swing;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import treinaweb.javabasico.eventos.EventoTextField;
import treinaweb.javabasico.eventos.EventosButton;

public class Programa {

	public static void main(String[] args) {
		
		//Janela
		JFrame frame = new JFrame(); //criando janela
		frame.setSize(400, 200); //configurando tamanho da janela
		frame.setLocation(400, 150); //configurando a localizacao da janela
		frame.setTitle("Grafico com Swing"); //titulo da janela
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //fechar a janela e encerra aplicacao quando for executado
		//frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE); //fechar a janela mais nao encerra aplicacao
		
		// Painel
		JPanel panel = new JPanel(); //criando painel
		panel.setLayout(null); //definindo layout
		
		// Label
		JLabel label = new JLabel("Texto");
		label.setBounds(10, 10, 60, 20); // largura, altura, tamanho, comprimento
		panel.add(label); //adcional no painel
		
		// Campo de Texto
		JTextField campo = new JTextField(); 
		campo.setBounds(50, 10, 200, 20); //largura, altura, tamanho, comprimento
		EventoTextField eventoTexto = new EventoTextField(); //chamar evento ao campo de texto
		campo.addKeyListener(eventoTexto); //adicionar o evento no campo de texto
		panel.add(campo); //adcional no painel
		
		//Botao
		JButton botao = new JButton("Processar");
		botao.setBounds(260, 10, 100, 20); //largura, altura, tamanho, comprimento
		EventosButton eventoBotao = new EventosButton(campo); //chamar evento
		botao.addActionListener(eventoBotao); //adicionar o evento ao botao
		panel.add(botao); //adcional no painel
		
		frame.add(panel); //adicionar painel ao frame
		
		frame.setVisible(true); //tornar visivel 

		
	}

}
