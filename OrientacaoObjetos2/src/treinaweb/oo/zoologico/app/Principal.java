package treinaweb.oo.zoologico.app;

import treinaweb.oo.java.basico.zoologico.Animal;

public class Principal {

	public static void main(String[] args) {
		
		Animal 	cachorro = new Animal("Toto", 5, "Vira Lata"); //adicionar atributos ultilizando metodo construtor da classe
		Animal.QUANTIDADE_ANIMAIS++;
		
		try { //executar procedimento abaixo
		
			//adicionar atributos do animal
	/*		cachorro.setNome("Toto");
			cachorro.setIdade(2);
			cachorro.setEspecie("Vira Lata"); */
			
			//pegar os atributos da classe Animal
			System.out.println(cachorro.getNome());
			System.out.println(cachorro.getIdade());
			System.out.println(cachorro.getEspecie());
			System.out.println(cachorro.isEstaVivo());
			//pegar os metodos da classe Animal
			cachorro.Comer();
			cachorro.Dormir();
			System.out.println(cachorro.Crescer());
			Animal.VerificarEstaVivo(cachorro);
			cachorro.EmitirBarrulho();
			cachorro.EmitirBarrulho("AU AU AU");
			
			System.out.println("\nQuantidade de animais Cadastrados: " + Animal.QUANTIDADE_ANIMAIS);
			
		}catch (Exception exececao) { //caso ocorra erro pegar o erro, tratamento de erro
			System.out.println(exececao.getMessage()); //exibir a mensagem de exececao
		}

		
		
	}

}
