package treinaweb.oo.java.basico.zoologico;

//set Usamos set para definir valores. Esse tipo de m�todo geralmente n�o retorna valores

//get Usamos get para obter informa��es. Esse tipo de m�todo sempre retorna um valor.

public class Animal {
	
	public static int QUANTIDADE_ANIMAIS = 0;
	
	//Atributos da classe
	private String nome = "";
	private int idade;
	private String especie = "";
	private boolean estaVivo = true;
	
	
	
	public Animal(String nome, int idade, String especie) { //Metodo Construtor da classe
		this.nome = nome; //nome da classe igual a nome parametro
		this.idade = idade; //idade da classe igual a idade parametro
		this.especie = especie; //especie da classe igual a especia parametro
	}

	//Metodos da classe
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) throws Exception {
		if(nome.equals("")) { //se nome for vazio tratar exececao
			throw new Exception("O nome nao pode estar vazio"); //nome nao pode ser vazio
		}
		
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getEspecie() {
		return especie;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}

	public boolean isEstaVivo() {
		return estaVivo;
	}

	public void setEstaVivo(boolean estaVivo) {
		this.estaVivo = estaVivo;
	}
	
	public String Crescer() {
		return nome + " O animal esta crescendo...";
	}
	
	public void Dormir() {
		System.out.println(nome + " O animal esta dormindo");
	}
	
	public void Comer() {
		System.out.println(nome + " O animal esta comendo");
	}
	 
	public void EmitirBarrulho() { //exemplo de sobrecarga de metodo
		System.out.println("Barruho do animal: nknllmlmlm");
	}
	
	public void EmitirBarrulho(String barrulho) { //exemplo de sobrecarga de metodo
		System.out.println("Barrulho do animal: " + barrulho);
	}
	
	
	public static void VerificarEstaVivo(Animal animal) {  //metodo estatico
		System.out.println(animal.isEstaVivo() ? animal.getNome() + " esta vivo!" : animal.getNome() + " morreu!");
	}                             //operador ternario, entao(?) se nao(:) 
	

}
