package treinaweb.tratamentoexcecoes.javabasico;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Exececao4 {

	public static void main(String[] args) {
		
		int numero;
		
		try { //executar procedimento abaixo
			Scanner leitor = new Scanner(System.in);
			
			System.out.printf("Digite o numero: ");
			numero = leitor.nextInt();
			System.out.printf("%d X 2 = %d", numero, numero * 2);
		} catch(InputMismatchException exececao) { //InputMismatchException caso ocorra erro nos dados de entrada
			System.out.println("Vc digitou numero errado."); //informar o erro
		}catch(Exception exececao) { //caso ocorra erro pegar o erro, tratamento de erro
			System.out.println("Ocorreu um erro.");
		} finally { //sempre sera executado, 
			System.out.println("Aplicacao encerrada!");
		}
	}

}
