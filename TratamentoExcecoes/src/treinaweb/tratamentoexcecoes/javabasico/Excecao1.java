package treinaweb.tratamentoexcecoes.javabasico;

public class Excecao1 {

	public static void main(String[] args) {
		
		int i = 5;
		int x = 0;
		int r = 0;
		
		try { //executar procedimento abaixo
			r = i / x; //ira ocorre um erro, nao se pode dividir por zero
			System.out.println("r = " +r);
		} catch(Exception excecao){ //caso ocorra erro pegar o erro
			System.out.println("Ocorreu um erro."); //mostrar que ocorreu erro
		}
		
		System.out.println("r = " + r);

	}

}
