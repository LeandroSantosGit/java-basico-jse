package treinaweb.tratamentoexcecoes.javabasico;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Exececao5 {
	

	public static void main(String[] args) {
		
		Scanner leitor = new Scanner(System.in);
		
		try { //executar procedimento abaixo
			
			int numero = LerNumero(leitor);
			
			System.out.printf("2 / %d = %d", numero, DividirNumeros(numero));
		} catch(InputMismatchException exececao) { //InputMismatchException caso ocorra erro nos dados de entrada
			System.out.println("Vc digitou numero errado."); //informar o erro
		}catch(Exception exececao) { //caso ocorra erro pegar o erro, tratamento de erro
			System.out.println("Ocorreu um erro.");
		} finally { //sempre sera executado, 
			System.out.println("\nAplicacao encerrada!");
		}
	
	}
	
	private static int LerNumero(Scanner leitor) {
		System.out.printf("Digite o numero: ");
		return leitor.nextInt();
	}
	
	private static int DividirNumeros(int numero) throws ArithmeticException{ //thows ArithmeticException informa que esse metodo 
		 numero = numero / 2;                                                 // pode ocorrer um erro aritmetico
		 
		 return numero;
	}  

}
