package treinaweb.tratamentoexcecoes.javabasico;

public class Excecao2 {

	public static void main(String[] args) {

		int resposta = 0;
		
		try { //executar procedimento abaixo
			resposta = obterDivisao(6, 0); //passamos os valores para num1 e num2
		}catch(Exception excesao) { //caso ocorra erro pegar o erro
			System.out.println("Erro: " + excesao.getMessage()); //getMessage retorna a mensagem original da excecao 
			System.out.println("Erro2: " + excesao.getStackTrace());//getStackTrace retorna as linhas do codigo aonde ocorre o erro
			System.out.println("Erro3: " + excesao.getCause()); //retorna a causa, o porque ocorreu a excesao
		}
		
		System.out.println("\nResposta: " + resposta); //o resultado sem ser tratado na execesao
	}
	
	static int obterDivisao(int num1, int num2) throws Exception{ //criamos um metode de divisao e passando throws que informa que 
		                                                          // pode ocorre um erro na operaca, ou seja se o usuario digitar
		int r = 0;                                                // zero ocorrera erro
		
		r = num1 / num2; 
		
		return r;
	}

}
